# Changelog

## 2.0.0 
- changed blade `{{ $varname or null }}` to `{{ $varname ?? null }}` to work with Laravel 6
- updated readme.md

## 2.0.1
- `vendor:publish`: changed tag name from `config` to `cookie-consent-config`
- updated readme.md concerning changes made to `vendor:publish`

## 2.0.2 / 2.0.3 
- improved documentation in config and readme

## 2.0.4 
- Solved bug in `form.blade.php`: `trying to access array offset on value of type null`

## 2.0.5 
- Solved bug in `BladeDirectives/CookieConsent.php:25`: `Undefined index` 

## 2.0.6
- moved changelog to project root
- fix for ErrorException `Undefined index` when there are unexpected values in the `$consentedChannels` 
- removed unneeded namespaces
- fixed typo in Middleware namespace
- defined dependencies in composer.json

## 2.0.7
- fixed case sensitive issue with the middleware to prepare for composer 2.0
- added `use Illuminate\Routing\Router` to ServideProvider

## 2.0.8
- fixed case sensitive issue with the middleware to prepare for composer 2.0 (again but properly)

## 2.0.8
- Solved another bug in `form.blade.php`: `trying to access array offset on value of type null`. same bug as in `2.0.4`

## 2.0.12
- Laravel 10 compatible