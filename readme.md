# How to install

## Composer 
Add this to your projects composer.json file:

```json
{   
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:reyez/cookie_consent.git"
        }
    ]
}
```

Then run `composer require reyez/cookie_consent:^2.0.0` 

## Include service provider (not needed for Laravel >= 5.5)
In config/app.php
```php
'providers' => [
    \Reyez\CookieConsent\ServiceProvider::class,
],
```

## Middleware
Add the `\Reyez\CookieConsent\Middleware\CookieConsentMiddleware::class` middleware to your `app/Http/Kernel.php`
Add it at the bottom to the global middleware group

## Publish config (mandatory)
Execute `php artisan vendor:publish --tag=cookie-consent-config`

## Publish views (optional)
Execute `php artisan vendor:publish --tag=cookie-consent-views`

## Publish translations (optional)
Execute `php artisan vendor:publish --tag=cookie-consent-translations`

## Blade template
Include the blade template as low possible in your projects main template: `@include('cookie-consent::cookie-bar')`

## Readmore route
There MUST be a route defined in the `config/cookie-consent.php` file
This can be:
- a route name
- an url
- or implement the `Reyez\CookieConsent\RouteResolver\Contracts\RouteResolver` interface

```php
<?php

class ServiceProvider extends Illuminate\Support\ServiceProvider
{
    public function register()
    {
        $this->app->bind(Reyez\CookieConsent\RouteResolver\Contracts\RouteResolver::class, RouteResolver\RouteResolver::class);
    }
}
```

```php
<?php

// Al the room you need to implement your own route resolve logic
class RouteResolver implements Reyez\CookieConsent\RouteResolver\Contracts\RouteResolver
{
    public function resolveReadMore(): ?string
    {
        $langCode = 'nl-nl';

        return route('cookie-consent-info-' . $langCode);
    }

    public function resolveSettings(): ?string
    {
        $langCode = 'nl-nl';

        return route('cookie-consent-settings-' . $langCode);
    }
}
```

## Cookie settings page
This will work out of the box. But if you need to adjust you van overrule the route, controller and view.

## Minimum example 
```html
@cookieconsent('video')
<video></video>
@endcookieconsent
```

## Example with a placeholder
```html
@cookieconsent('video')
<video></video>
@else
<div class="video-placeholder">
    You must first give consent if you want to watch this video
</div>
@endcookieconsent
```

# Ajax submit
It is possible to give consent without reloading the page.
however, it requires some more work to implement.

First enable the ajax submit in the config.


## minimum html
```html
@cookieconsent('video')
    <div class="video">
        <video></video>
    </div>
@else
    <div class="video-placeholder js-cookie-consent-dependency" data-channel-name="video">
        <span>@lang('Om deze video te bekijken moet je eerst met de cookie melding akkoord gaan')</span>
    </div>
    @openScripttemplate
    <div class="video">
        <video></video>
    </div>
    @closeScripttemplate
@endcookieconsent
```

## more advanced html (you can place the target everywhere on the page instead of direct sibling of the placeholder)
```html
@cookieconsent('video')
<div class="video">
    <video></video>
</div>
@else
    <div class="video video--no-cookie-consent js-cookie-consent-dependency" 
        data-target="cookieConsentDependency_videoPdp_{{ $video->videoCode }}" 
        data-js-func="processVideos"
        data-channel-name="video"
    >
        <span>Om deze video te bekijken moet je eerst met de cookie melding akkoord gaan</span>
    </div>
    @openScripttemplate('cookieConsentDependency_videoPdp_', $awards->videoCode )
    <div class="video">
        <video></video>
    </div>
    @closeScripttemplate
@endcookieconsent
```

- `<meta name="csrf-token" content="{{ csrf_token() }}">` mandatory: is needed for Ajax posting. place this somewhere in your `<head>` 
- mandatory: This class denotes a dependency placeholder: `js-cookie-consent-dependency`.
- `data-channel-name` mandatory: holds the channel name. ie: 'video' or 'social'
- `data-js-func` optional: is the name of a function in global scope that is called after the dependency is placed in the DOM. This is so you can do some initialising
- `data-target` optional: holds the id of the template that holds the dependency. if not provided the will assume the script tag is a direct sibling of the placeholder


# Blade directives in Phpstorm

Go inside preferences: `Preferences | Languages & Frameworks | PHP | Blade` and click the tab called `directives`

Add the following directives:

- `cookieconsent`           `<?php if(`        `): ?>`
- `elsecookieconsent`       `<?php elseif(`    `): ?>`
- `endcookieconsent`
- `openScripttemplate`      `<?php echo`       `?>`
- `closeScripttemplate`     `<?php echo`       `?>`
- `cookieconsentCmstext`    `<?php echo`       `?>`


# Issues and possible solutions

## If the cookiebar shows but won't dissapear after consent
Check the devtools and watch if the cookie value is readable. If not the order of the middleware is probably not correct.
