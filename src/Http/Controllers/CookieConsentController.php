<?php

namespace Reyez\CookieConsent\Http\Controllers;

use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Routing\Redirector;
use Reyez\CookieConsent\Helpers\CookieName;
use Reyez\CookieConsent\Parser\Contracts\Writer;

class CookieConsentController extends BaseController
{
    protected $cookieJar;
    protected $cookieParser;
    protected $cookieName;

    public function __construct(CookieJar $cookieJar, Writer $cookieParser, CookieName $cookieName)
    {
        $this->cookieJar = $cookieJar;
        $this->cookieParser = $cookieParser;
        $this->cookieName = $cookieName;
    }

    public function index(Request $request, Response $response, Redirector $redirect)
    {
        $content = $this->cookieParser->write($request->get('channels'));
        $cookie = $this->cookieJar->forever($this->cookieName->getCookieName(), $content);

        if ($request->ajax()) {
            $response->withHeaders([
                'Content-Type' => 'application/json',
            ]);
            $response->setContent($content);
        } else {
            $response = $redirect->back();
        }

        $response->withCookie($cookie);

        return $response;
    }

    public function settings()
    {
        return view('cookie-consent::settings');
    }
}
