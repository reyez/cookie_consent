<?php namespace Reyez\CookieConsent\BladeDirectives;

class ScriptTemplate implements Contracts\ScriptTemplate
{
    /**
     * @param $id
     * @return string
     */
    public function start($id)
    {
        if ($id) {
            return "<?php echo '<script type=\"text/plain\" id=\"'.$id.'\">' ?>";
        }

        return "<?php echo '<script type=\"text/plain\">' ?>";
    }

    public function end()
    {
        return '</script>';
    }
}
