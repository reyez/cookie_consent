<?php namespace Reyez\CookieConsent\BladeDirectives;

use Illuminate\Http\Request;
use Reyez\CookieConsent\Parser\Contracts\Reader;

class CookieConsent implements Contracts\CookieConsent
{
    protected $cookieContents;

    public function __construct(Request $request, Reader $cookieParser)
    {
        $this->cookieContents = $cookieParser->read($request);
    }

    /**
     * @param $channel
     * @return bool
     */
    public function ifConsent($channel)
    {
        if (is_null($this->cookieContents)) {
            return false;
        }

        return isset($this->cookieContents[$channel]) && $this->cookieContents[$channel] === true;
    }
}
