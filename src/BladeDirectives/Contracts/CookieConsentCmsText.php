<?php namespace Reyez\CookieConsent\BladeDirectives\Contracts;

interface CookieConsentCmsText
{
    /**
     * @param $cmsText
     * @return string
     */
    public function echoCmstText($cmsText);
}
