<?php namespace Reyez\CookieConsent\BladeDirectives\Contracts;

interface ScriptTemplate
{
    /**
     * @param $string
     * @return string
     */
    public function start($string);

    public function end();
}
