<?php namespace Reyez\CookieConsent\BladeDirectives\Contracts;

interface CookieConsent
{
    /**
     * @param $channel
     * @return bool
     */
    public function ifConsent($channel);
}
