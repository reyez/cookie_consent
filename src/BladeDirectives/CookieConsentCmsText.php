<?php namespace Reyez\CookieConsent\BladeDirectives;

use Reyez\CookieConsent\Replacer\Replacer;

class CookieConsentCmsText implements Contracts\CookieConsentCmsText
{
    /**
     * @param $cmsText
     * @return string
     */
    public function echoCmstText($cmsText)
    {
        $class = Replacer::class;

        return "<?php echo $class::replace($cmsText) ?>";
    }
}
