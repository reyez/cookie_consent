<?php namespace Reyez\CookieConsent\Helpers;

class CookieName
{
    /**
     * @param $name
     * @return bool
     */
    public function isCookieConsentCookie($name)
    {
        return $this->getCookieName() === $name;
    }

    /**
     * @return string
     */
    public function getCookieName()
    {
        return $this->constructCookieName(config('cookie-consent.cookie-name'));
    }

    /**
     * @param $cookieName
     * @return string
     */
    protected function constructCookieName($cookieName)
    {
        return $cookieName.'-'.$this->getSuffix();
    }

    /**
     * This suffix is needed so that when there is an channel added the cookie is discarded and the user is forced to
     * reexamen the updated cookie policy
     *
     * @return bool|string
     */
    protected function getSuffix()
    {
        $channels = config('cookie-consent.channels-status');

        if(is_null($channels)){
            $channels = [];
        }

        // get all possible channel names
        $arrayKeys = array_keys($channels);

        // make sure the hash is not dependent on the order of the channels
        sort($arrayKeys);

        // make a single string
        $concat = implode('', $arrayKeys);

        // make a suffix out of the concatenated array keys
        $suffix = substr(md5($concat), 0, 10);

        return $suffix;
    }
}
