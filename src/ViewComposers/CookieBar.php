<?php

namespace Reyez\CookieConsent\ViewComposers;

use Illuminate\Config\Repository as Config;
use Illuminate\View\View;
use Reyez\CookieConsent\RouteResolver\Contracts\RouteResolver;

class CookieBar
{
    protected $config;
    protected $routeResolver;

    public function __construct(Config $config, RouteResolver $routeResolver)
    {
        $this->config = $config;
        $this->routeResolver = $routeResolver;
    }

    public function compose(View $view)
    {
        $channels = $this->parseChannels($this->config->get('cookie-consent.channels-status'));
        $readMoreUrl = $this->routeResolver->resolveReadMore();
        $enableAjaxSubmit = $this->config->get('cookie-consent.enable-ajax-submit');

        if ($readMoreUrl) {
            $view->with('readMoreUrl', $readMoreUrl);
            $view->with('noReadOnRouteConfigured', false);
        } else {
            $view->with('noReadOnRouteConfigured', true);
        }

        $view->with('cookieConsentUrl', route('cookie-consent'));
        $view->with('enableAjaxSubmit', $enableAjaxSubmit);
        $view->with('channels', $channels);
    }

    protected function parseChannels($channelStatuses)
    {
        // filter al disabled channels
        $filteredChannels = array_keys(array_filter($channelStatuses, function ($enabled) {
            return $enabled;
        }));

        // get all translations
        $channelContent = array_map(function ($channel) {
            return [
                'title' => trans("cookie-consent::messages.$channel.title"),
                'text' => trans("cookie-consent::messages.$channel.text"),
            ];
        }, $filteredChannels);

        // return an assosiative array
        return array_combine($filteredChannels, $channelContent);
    }
}
