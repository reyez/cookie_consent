<?php

namespace Reyez\CookieConsent;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Reyez\CookieConsent\Middleware\CookieConsentMiddleware;
use Reyez\CookieConsent\RouteResolver;
use Reyez\CookieConsent\ViewComposers\CookieBar;

class ServiceProvider extends BaseServiceProvider
{
    const NAMESPACE_NAME = 'cookie-consent';
    const ROUTES_PATH = __DIR__ . '/routes.php';
    const CONFIG_PATH = __DIR__ . '/config.php';
    const VIEW_PATH = __DIR__ . '/resources/views';
    const TRANSLATIONS_PATH = __DIR__ . '/resources/lang';

    public function boot()
    {
        $this->bootRoutes();
        $this->bootConfig();
        $this->bootBlade();
        $this->bootView();
        $this->bootTranslations();
        $this->bootMiddleware();
    }

    public function register()
    {
        $this->app->bind(BladeDirectives\Contracts\ScriptTemplate::class, BladeDirectives\ScriptTemplate::class);
        $this->app->bind(BladeDirectives\Contracts\CookieConsent::class, BladeDirectives\CookieConsent::class);
        $this->app->bind(BladeDirectives\Contracts\CookieConsentCmsText::class, BladeDirectives\CookieConsentCmsText::class);

        $this->app->bind(Parser\Contracts\Writer::class, Parser\Writer::class);
        $this->app->bind(Parser\Contracts\Reader::class, Parser\Reader::class);

        $this->app->bind(RouteResolver\Contracts\RouteResolver::class, RouteResolver\RouteResolver::class);
    }

    protected function bootRoutes()
    {
        if (method_exists($this, 'loadRoutesFrom')) {
            // INFO: loadRoutesFrom is available since Laravel 5.3
            $this->loadRoutesFrom(self::ROUTES_PATH);
        } elseif (!$this->app->routesAreCached()) {
            require self::ROUTES_PATH;
        }
    }

    protected function bootConfig()
    {
        $this->publishes([
            self::CONFIG_PATH => config_path('cookie-consent.php'),
        ], 'cookie-consent-config');
    }

    protected function bootBlade()
    {
        $scriptTemplate = app()->make(BladeDirectives\Contracts\ScriptTemplate::class);
        $cookieConsentDirective = app()->make(BladeDirectives\Contracts\CookieConsent::class);
        $cookieConsentCmsTextDirective = app()->make(BladeDirectives\Contracts\CookieConsentCmsText::class);

        if (method_exists(Blade::getFacadeRoot(), 'if')) {
            // INFO: Custom If Statements are available since Laravel 5.5
            Blade::{'if'}('cookieconsent', function ($text) use ($cookieConsentDirective) {
                return $cookieConsentDirective->ifConsent($text);
            });
        }

        Blade::directive('openScripttemplate', function ($text = null) use ($scriptTemplate) {
            return $scriptTemplate->start($text);
        });

        Blade::directive('closeScripttemplate', function () use ($scriptTemplate) {
            return $scriptTemplate->end();
        });

        Blade::directive('cookieconsentCmstext', function ($cmsText) use ($cookieConsentCmsTextDirective) {
            return $cookieConsentCmsTextDirective->echoCmstText($cmsText);
        });
    }

    protected function bootView()
    {
        view()->composer(self::NAMESPACE_NAME . '::cookie-bar', CookieBar::class);
        view()->composer(self::NAMESPACE_NAME . '::parts.form', CookieBar::class);

        $this->loadViewsFrom(self::VIEW_PATH, self::NAMESPACE_NAME);

        $this->publishes([
            self::VIEW_PATH => resource_path('views/vendor/' . self::NAMESPACE_NAME),
        ], self::NAMESPACE_NAME . '-views');
    }

    protected function bootTranslations()
    {
        $this->loadTranslationsFrom(self::TRANSLATIONS_PATH, self::NAMESPACE_NAME);

        $this->publishes([
            self::TRANSLATIONS_PATH => resource_path('lang/vendor/' . self::NAMESPACE_NAME),
        ], self::NAMESPACE_NAME . '-translations');
    }

    protected function bootMiddleware()
    {
        /** @var Router $router */
        $router = $this->app['router'];
        $router->prependMiddlewareToGroup('web', CookieConsentMiddleware::class);
    }
}
