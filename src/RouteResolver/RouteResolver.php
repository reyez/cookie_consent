<?php namespace Reyez\CookieConsent\RouteResolver;

use Illuminate\Config\Repository as Config;
use Illuminate\Support\Facades\Route;

class RouteResolver implements Contracts\RouteResolver
{
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @return string|null
     */
    public function resolveReadMore()
    {
        return $this->resolve('cookie-consent.read-more-route');
    }

    /**
     * @return string|null
     */
    public function resolveSettings()
    {
        return $this->resolve('cookie-consent.settings-route');
    }

    /**
     * @param $key
     * @return string|null
     */
    protected function resolve($key)
    {
        $route = $this->config->get($key);

        if (Route::has($route)) {
            return route($route);
        } elseif (is_string($route)) {
            return $route;
        }

        return null;
    }
}
