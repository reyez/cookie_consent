<?php namespace Reyez\CookieConsent\RouteResolver\Contracts;

interface RouteResolver
{
    /**
     * @return string|null
     */
    public function resolveReadMore();

    /**
     * @return string|null
     */
    public function resolveSettings();
}

