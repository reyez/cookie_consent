<?php namespace Reyez\CookieConsent\Replacer;

class Replacer
{
    /**
     * @param $cmsText
     * @return string|null
     */
    public static function replace($cmsText)
    {
        $forceUtf8 = '<?xml encoding="utf-8" ?>'; // This is a hack so DomDocument handles UTF8 properly

        // suppress mallformed HTML errors
        libxml_use_internal_errors(true);

        $dom = new \DOMDocument;

        $dom->loadHTML($forceUtf8 . $cmsText, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        self::replaceVideos($dom);

        $html = $dom->saveHtml();

        // prevent memory leak by clearing possible errors
        libxml_clear_errors();

        
        return substr($html, strlen($forceUtf8));
    }

    /**
     * @param \DOMDocument $document
     */
    protected static function replaceVideos(\DOMDocument $document)
    {
        $iframes = $document->getElementsByTagName('iframe');

        $matchingDomains = ['www.youtube.com', 'player.vimeo.com'];
        $view = 'cookie-consent::cmstext.video';

        foreach ($iframes as $iframe) {
            $domain = parse_url($iframe->getAttribute('src'), PHP_URL_HOST);

            if (!in_array($domain, $matchingDomains)) {
                return;
            }

            $html = view($view, [
                'video' => $document->saveHtml($iframe),
            ]);

            $wrapper = $document->createDocumentFragment();
            $wrapper->appendXML($html); // wrap the iframe

            // replace iframe with wrapped one
            $iframe->parentNode->replaceChild($wrapper, $iframe);
        }
    }
}
