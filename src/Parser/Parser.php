<?php namespace Reyez\CookieConsent\Parser;

use Reyez\CookieConsent\Helpers\CookieName;

abstract class Parser
{
    protected $cookieName;

    public function __construct(CookieName $cookieName)
    {
        $this->cookieName = $cookieName;
    }
}
