<?php namespace Reyez\CookieConsent\Parser;

use Illuminate\Http\Request;

class Reader extends Parser implements Contracts\Reader
{
    /**
     * @param Request $request
     * @return array|null
     */
    public function read(Request $request)
    {
        $cookieName = $request->cookie($this->cookieName->getCookieName());

        $cookie = is_string($cookieName) ? $cookieName : '';

        return json_decode($cookie, true);
    }
}
