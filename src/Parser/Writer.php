<?php namespace Reyez\CookieConsent\Parser;

class Writer extends Parser implements Contracts\Writer
{
    /**
     * @param $consentedChannels
     * @return string
     */
    public function write($consentedChannels)
    {
        $content = [];

        foreach ($this->getEnabledChannels() as $channel) {
            $enabled = $consentedChannels === 'all' || isset($consentedChannels[$channel]) && $consentedChannels[$channel] === 'true';

            $content[$channel] = $enabled;
        }

        return json_encode($content);
    }

    /**
     * @return array
     */
    protected function getEnabledChannels()
    {
        $channels = config('cookie-consent.channels-status');

        return array_keys(array_filter($channels, function ($enabled) {
            return $enabled;
        }));
    }
}
