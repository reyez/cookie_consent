<?php namespace Reyez\CookieConsent\Parser\Contracts;

use Illuminate\Http\Request;

interface Reader
{
    /**
     * @param Request $request
     * @return array|null
     */
    public function read(Request $request);
}
