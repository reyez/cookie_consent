<?php namespace Reyez\CookieConsent\Parser\Contracts;

interface Writer
{
    /**
     * @param $consentedChannels
     * @return mixed
     */
    public function write($consentedChannels);
}
