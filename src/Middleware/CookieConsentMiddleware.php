<?php

namespace Reyez\CookieConsent\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\View\Factory as ViewFactory;
use Reyez\CookieConsent\Parser\Contracts\Reader;

class CookieConsentMiddleware
{
    protected $cookieParser;
    protected $view;

    public function __construct(Reader $cookieParser, ViewFactory $view)
    {
        $this->cookieParser = $cookieParser;
        $this->view = $view;
    }

    public function handle(Request $request, Closure $next)
    {
        $consent = $this->cookieParser->read($request);

        $this->view->share('cookieConsentChannels', $consent);
        $this->view->share('noCookieConsent', is_null($consent));

        return $next($request);
    }
}
