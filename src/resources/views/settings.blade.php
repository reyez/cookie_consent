<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cookie consent</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style>
        *,
        ::before,
        ::after {
            box-sizing: border-box;
            }
        body {
            margin: 0;
            }
        .wrapper {
            max-width: 1200px;
            margin: 0 auto;
            }
        button {
            cursor: pointer;
            }
    </style>
    @include('cookie-consent::parts.css')
</head>
<body>
    <div class="wrapper">
        <h3 class="cookie-bar-panel__title">
            {{ trans('cookie-consent::messages.panel-title') }}
        </h3>

        <p class="cookie-bar-panel__text">
            {{ trans('cookie-consent::messages.panel-text') }}
        </p>
        @include('cookie-consent::parts.form')
    </div>
</body>
</html>
