<script>
(function openPanel(clicker){
    if(!clicker)return;
    var panel = document.getElementById(clicker.getAttribute('data-target'));
    var isShownClass = panel.getAttribute('data-is-shown-class');
    var closeBtn = panel.querySelector(panel.getAttribute('data-close-btn-selector'));

    clicker.addEventListener('click', open);
    closeBtn.addEventListener('click', close);

    function open(){
        panel.classList.add(isShownClass);
    }
    function close(){
        panel.classList.remove(isShownClass);
    }
})(document.querySelector('.js-cookie-bar-settings-clicker'));

@if($enableAjaxSubmit)
(function processDependencies(container, dependencies){
    var clicker = container.querySelector(container.getAttribute('data-clicker-selector'));
    var disappearClass = container.getAttribute('data-dissapear-class');
    var form = container.querySelector('form');
    var template = document.createElement('div');
    var CSRFToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

    clicker.addEventListener('click', onClick);
    form.addEventListener('submit', onSubmit);

    function onClick(e){
        e.preventDefault();
        doRequest('GET', clicker.href, null, onSuccess);
    }
    function onSubmit(e){
        e.preventDefault();
        doRequest('POST', form.action, new FormData(form), onSuccess);
    }
    function doRequest(method, url, data, successCallback){
        var request = new XMLHttpRequest();
        request.open(method, url, true);
        request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        request.setRequestHeader('X-CSRF-TOKEN', CSRFToken);
        request.onload = function _load() {
            if (this.status >= 200 && this.status < 400) successCallback.call(this, JSON.parse(this.response));
        };
        request.send(data);
    }
    function onSuccess(channels){
        container.classList.add(disappearClass);
        processDependencies(channels);
    }
    function processDependencies(channels){
        var i = dependencies.length;
        for(;i--;) processDependency(dependencies[i], channels);
    }
    function processDependency(dependency, channels){
        var htmlContainer = dependency.getAttribute('data-target')
            ? document.getElementById(dependency.getAttribute('data-target')) : dependency.nextElementSibling;
        var funcName = dependency.getAttribute('data-js-func');
        var channelName = dependency.getAttribute('data-channel-name');
        if(!channels[channelName]) return; // we have no consent!
        template.innerHTML = htmlContainer.innerHTML;
        dependency.parentNode.replaceChild(template.firstElementChild, dependency);
        htmlContainer.parentNode.removeChild(htmlContainer);
        if(typeof window[funcName] === 'function') window[funcName]();
    }
})(document.querySelector('.js-cookie-bar'), document.querySelectorAll('.js-cookie-consent-dependency'));
@endif
</script>
