<form method="get" action="{{ $cookieConsentUrl }}">
@if(isset($inCookieBar) && $inCookieBar)
    <button class="cookie-bar-panel__close cookie-bar-panel-close" type="button">
        <span class="cookie-bar-panel-close__icon"><svg width="32" height="32" viewBox="0 0 357 357" xmlns="http://www.w3.org/2000/svg"><path d="m357 35.7-35.7-35.7-142.8 142.8-142.8-142.8-35.7 35.7 142.8 142.8-142.8 142.8 35.7 35.7 142.8-142.8 142.8 142.8 35.7-35.7-142.8-142.8z"/></svg></span>
        <span class="is-visually-hidden">{{ trans('cookie-consent::messages.close-button') }}</span>
    </button>

    <h3 class="cookie-bar-panel__title">{{ trans('cookie-consent::messages.panel-title') }}</h3>

    <p class="cookie-bar-panel__text">
        {{ trans('cookie-consent::messages.panel-text') }}
    </p>
@endIf

@if($noReadOnRouteConfigured)
    <span class="cookie-bar-panel__read-more cookie-bar-read-more" style="color: red;">No readon route configured!</span>
@else
    <a class="cookie-bar-panel__read-more cookie-bar-read-more" href="{{ $readMoreUrl  ?? null }}">{{ trans('cookie-consent::messages.read-more-button') }}</a>
@endif
@foreach($channels as $channel => $channelContent)
    <fieldset class="cookie-bar-panel__channel cookie-bar-channel">
        <legend class="cookie-bar-channel__name">{{ $channelContent['title'] }}</legend>
        <div class="cookie-bar-channel__options">
            <input class="is-visually-hidden" type="radio" name="channels[{{ $channel }}]" value="true" id="frmInputCookieBar{{ $channel }}True" @if($cookieConsentChannels && $cookieConsentChannels[$channel]) checked @endif>
            <label class="cookie-bar-channel-option cookie-bar-channel-option--true" for="frmInputCookieBar{{ $channel }}True">
                {{ trans('cookie-consent::messages.yes') }}
            </label>
            <input class="is-visually-hidden" type="radio" name="channels[{{ $channel }}]" value="false" id="frmInputCookieBar{{ $channel }}False" @if(!$cookieConsentChannels || $cookieConsentChannels && !$cookieConsentChannels[$channel]) checked @endif>
            <label class="cookie-bar-channel-option cookie-bar-channel-option--false" for="frmInputCookieBar{{ $channel }}False">
                {{ trans('cookie-consent::messages.no') }}
            </label>
        </div>
        <div class="cookie-bar-channel__text">
            {{ $channelContent['text'] }}
        </div>
    </fieldset>
@endForeach

    <button class="cookie-bar__submit cookie-bar-submit">
        <span class="cookie-bar-submit__icon"><svg height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="m20.285 2-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z"/></svg></span>
        <span class="cookie-bar-submit__text">{{ trans('cookie-consent::messages.save') }}</span>
    </button>
</form>
