<style>
.is-visually-hidden {
    position: absolute;
    overflow: hidden;
    clip: rect(0 0 0 0);
    height: 1px;
    width: 1px;
    margin: -1px;
    padding: 0;
    border: 0;
    }
.cookie-bar {
    width: 100%;
    padding: 15px;
    position: fixed;
    bottom: 0;
    left: 0;
    z-index: 1000;
    background-color: white;
    box-shadow: 0 0 3px 0 rgba(0, 0, 0, .1);
    transition: opacity .2s,
    transform .2s;
    }

.cookie-bar__text {
    margin: 0 0 .5em 0;
    padding-right: 30px;
    }

.cookie-bar :link,
.cookie-bar :visited {
    text-decoration: none;
    }

.cookie-bar-read-more:link,
.cookie-bar-read-more:visited,
.cookie-bar__setting-btn {
    color: rgb(255, 185, 0);
    font-weight: bold;
    }

.cookie-bar__cta {
    display: inline-block;
    padding: .5em;
    border-radius: 3px;
    background-color: rgb(105, 190, 40);
    color: white;
    text-align: center;
    padding: .5em;
    }

.cookie-bar :link:focus,
.cookie-bar :visited:focus,
.cookie-bar :link:hover,
.cookie-bar :visited:hover,
.cookie-bar__setting-btn:focus,
.cookie-bar__setting-btn:hover {
    text-decoration: underline;
    }

.cookie-bar__setting-btn {
    border: 0;
    padding: .5em;
    margin: 0;
    background-color: transparent;
    text-align: center;
    }

.cookie-bar--disappear {
    opacity: 0;
    transform: translateY(100%);
    }

.cookie-bar::after {
    content: '';
    display: table;
    clear: right;
    }

.cookie-bar__btn {
    float: right;
    }

.cookie-bar__panel {
    width: 100%;
    min-height: 100px;
    position: absolute;
    bottom: 0;
    left: 0;
    transform: translateY(100%);
    background-color: white;
    box-shadow: 0 0 3px 0 rgba(0, 0, 0, .1);
    transition: transform .2s;
    }

.cookie-bar__panel--is-shown {
    transform: translateY(0);
    }

.cookie-bar-submit {
    border: 0;
    padding: .5em;
    margin: 0;
    border-radius: 3px;
    background-color: rgb(105, 190, 40);
    color: white;
    text-align: center;
    }

.cookie-bar-submit__icon {
    display: inline-block;
    vertical-align: middle;
    }

.cookie-bar-submit__icon * {
    fill: white;
    }

:hover > .cookie-bar-submit__text,
:focus > .cookie-bar-submit__text {
    text-decoration: underline;
    }

.cookie-bar-panel__read-more {
    display: inline-block;
    margin-bottom: 15px;
    }

.cookie-bar-panel__close {
    float: right;
    }

.cookie-bar-panel-close {
    border: 0;
    padding: 0;
    margin: 0;
    background-color: transparent;
    }

.cookie-bar-channel {
    border: 0;
    padding: 0 0 10px 0;
    margin: 0;
    }

.cookie-bar-panel__channel:last-of-type {
    border-bottom: 1px solid rgb(200,200,200);
    margin-bottom: 15px;
    }

.cookie-bar-channel::after {
    conent: '';
    display: table;
    clear: left;
    }

.cookie-bar-channel__name {
    width: 100%;
    margin: 0;
    padding: 5px 0 0 100px;
    border-top: 1px solid rgb(200, 200, 200);
    font-weight: bold;
    }

.cookie-bar-channel__text {
    max-width: 600px;
    }

.cookie-bar-channel__options {
    width: 100px;
    height: 50px;
    float: left;
    }

.cookie-bar-channel-option {
    display: inline-flex;
    width: 40px;
    height: 40px;
    border-radius: 50%;
    border: 1px solid rgb(200, 200, 200);
    align-items: center;
    justify-content: center;
    cursor: pointer;
    }

.cookie-bar-channel-option:hover,
:focus + .cookie-bar-channel-option {
    border-color: black;
    }

:checked + .cookie-bar-channel-option--true {
    background-color: green;
    }
:checked + .cookie-bar-channel-option--false {
    background-color: red;
    }

@media screen and (min-width: 500px)
{
    .cookie-bar {
        display: flex
        }

    .cookie-bar__setting-btn {
        width: 100%;
        }

    .cookie-bar__cta {
        width: 100%;
        }

    .cookie-bar-panel {

        }
    }

@media screen and (min-width: 1200px)
{
    .cookie-bar,
    .cookie-bar__panel {
        padding: 15px calc(50% - (1200px / 2) + 30px);
        }

    .cookie-bar-panel__close {
        position: absolute;
        right: 15px;
        }
}
</style>
