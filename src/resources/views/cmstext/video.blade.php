@cookieconsent('video')
    {!! $video !!}
@else
    <div class="video-placeholder js-cookie-consent-dependency" data-channel-name="video">
        <span>@lang('Om deze video te bekijken moet je eerst met de cookie melding akkoord gaan')</span>
    </div>
    @openScripttemplate
        {!! $video !!}
    @closeScripttemplate
@endcookieconsent
