@if($noCookieConsent)

    @include('cookie-consent::parts.css')

    <div class="cookie-bar js-cookie-bar" data-clicker-selector=".cookie-bar__cta" data-dissapear-class="cookie-bar--disappear">

        <div class="cookie-bar__half">
            <p class="cookie-bar__text">
                {{ trans('cookie-consent::messages.text') }}
            </p>
            @if($noReadOnRouteConfigured)
                <span class="cookie-bar__read-more cookie-bar-read-more" style="color: red;">No readon route configured!</span>
            @else
                <a class="cookie-bar__read-more cookie-bar-read-more" href="{{ $readMoreUrl ?? null }}">{{ trans('cookie-consent::messages.read-more-button') }}</a>
            @endif
        </div>
        <div class="cookie-bar__half">
            <a class="cookie-bar__cta" href="{{ $cookieConsentUrl }}?channels=all">{{ trans('cookie-consent::messages.cta-button') }}</a>
            <button class="cookie-bar__setting-btn js-cookie-bar-settings-clicker" data-target="cookie-consent-panel">{{ trans('cookie-consent::messages.settings-button') }}</button>
        </div>
        <div class="cookie-bar__panel cookie-bar-panel" id="cookie-consent-panel" data-is-shown-class="cookie-bar__panel--is-shown" data-close-btn-selector=".cookie-bar-panel-close">
            @include('cookie-consent::parts.form', [
                'inCookieBar' => true
            ])
        </div>
    </div>

    @include('cookie-consent::parts.js')

@endIf
