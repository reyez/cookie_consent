<?php

return [
    'text' => 'We use functional, analytical cookies and third-party cookies to ensure that this website functions optimally.',
    'read-more-button' => 'More explanation about cookies',
    'cta-button' => 'Allow cookies',
    'settings-button' => 'Change cookie settings',
    'close-button' => 'close',
    'panel-title' => 'Cookies on this website',
    'panel-text' => 'We place functional cookies in order for this website to function properly and analytical cookies with which we can measure the use of the website. These cookies do not use personal data. Give your consent below for cookies that do process personal data.',
    'yes' => 'Yes',
    'no' => 'No',
    'save' => 'Save',

    'video' => [
        'title' => 'I want video',
        'text' => 'This allows you to watch videos on this website and allows the placement of cookies by video platforms, these platforms can follow you and use your internet behavior for other purposes.',
    ],

    'social' => [
        'title' => 'I want social media',
        'text' => 'This allows the placement of cookies by social media networks, these networks can follow you and use your internet behavior for other purposes.',
    ],

    'advertisement' => [
        'title' => 'I want personalized advertisements',
        'text' => 'With this you receive personalized advertisements that are tailored to your internet behavior. Via these cookies your internet behavior can be followed by advertising networks.',
    ],

    'tracking' => [
        'title' => 'Ich möchte verfolgen',
        'text' => 'With these cookies your behavior on this website can be monitored and used to improve the website.',
    ],
];
