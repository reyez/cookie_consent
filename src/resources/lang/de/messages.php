<?php

return [
    'text' => 'Wir verwenden Funktions-, Analyse- und Drittanbieter-Cookies, um sicherzustellen, dass diese Website optimal funktioniert.',
    'read-more-button' => 'Weitere Erklärung zu Cookies',
    'cta-button' => 'Cookies zulassen',
    'settings-button' => 'Cookie-Einstellungen ändern',
    'close-button' => 'schliessen',
    'panel-title' => 'Cookies auf dieser Website',
    'panel-text' => 'Wir setzen funktionale Cookies ein, damit diese Website ordnungsgemäß funktioniert, und analytische Cookies, mit denen wir die Nutzung der Website messen können. Diese Cookies verwenden keine personenbezogenen Daten. Geben Sie unten Ihre Zustimmung zu Cookies ein, die personenbezogene Daten verarbeiten.',
    'yes' => 'Ja',
    'no' => 'Nr.',
    'save' => 'Save',

    'video' => [
        'title' => 'Ich möchte ein Video',
        'text' => 'Dies ermöglicht Ihnen das Ansehen von Videos auf dieser Website und die Platzierung von Cookies durch Videoplattformen. Diese Plattformen können Ihnen folgen und Ihr Internetverhalten für andere Zwecke nutzen.',
    ],

    'social' => [
        'title' => 'Ich möchte soziale Medien',
        'text' => 'Dies ermöglicht die Platzierung von Cookies durch soziale Medien, diese Netzwerke können Ihnen folgen und Ihr Internetverhalten für andere Zwecke nutzen.',
    ],

    'advertisement' => [
        'title' => 'Ich möchte personalisierte Anzeigen',
        'text' => 'Damit erhalten Sie personalisierte Anzeigen, die auf Ihr Internetverhalten zugeschnitten sind. Über diese Cookies kann Ihrem Internetverhalten ein Werbenetzwerk folgen.',
    ],

    'tracking' => [
        'title' => 'Ich möchte {tracking}',
        'text' => 'Mit diesen Cookies kann Ihr Verhalten auf dieser Website überwacht und zur Verbesserung der Website verwendet werden.',
    ],
];
