<?php

return [
    'text' => 'We gebruiken functionele, analytische cookies en cookies van derden om ervoor te zorgen dat deze website optimaal functioneert.',
    'read-more-button' => 'Meer uitleg over cookies',
    'cta-button' => 'Cookies toestaan',
    'settings-button' => 'Cookie instellingen wijzigen',
    'close-button' => 'sluiten',
    'panel-title' => 'Cookies op deze website',
    'panel-text' => 'Wij plaatsen functionele cookies, om deze website naar behoren te laten functioneren en analytische cookies waarmee wij het gebruik van de website kunnen meten. Deze cookies gebruiken geen persoonsgegevens. Geef hieronder je toestemming voor cookies die wel persoonsgegevens verwerken.',
    'yes' => 'Ja',
    'no' => 'Nee',
    'save' => 'Opslaan',

    'video' => [
        'title' => 'Ik wil video koppelingen',
        'text' => 'Hiermee kun je video’s op deze website bekijken en staat het plaatsen van cookies door videoplatformen toe, deze platformen kunnen je volgen en jouw internetgedrag gebruiken voor andere doeleinden.',
    ],

    'social' => [
        'title' => 'Ik wil sociale media koppelingen',
        'text' => 'Hiermee staat het plaatsen van cookies door social medianetwerken toe, deze netwerken kunnen je volgen en jouw internetgedrag gebruiken voor andere doeleinden.',
    ],

    'advertisement' => [
        'title' => 'Ik wil gepersonaliseerde advertenties',
        'text' => 'Hiermee ontvang je gepersonaliseerde advertenties die worden afgestemd op jouw internetgedrag. Via deze cookies kan je internetgedrag gevolgd worden door advertentienetwerken.',
    ],

    'tracking' => [
        'title' => 'Ik wil tracking',
        'text' => 'Via deze cookies kan je gedrag op deze website worden gevolgd en gebruikt worden om de website te verbeteren.',
    ],
];
