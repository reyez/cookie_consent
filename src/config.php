<?php

// Consult the readme located at `../vendor/reyez/cookie_consent/readme.md` for documentation

return [
    // Name of the cookie wherein the permissions are stored
    'cookie-name' => 'cookie-consent',

    // NOTE: these are mandatory unless you provide your own RouteResolver (Reyez\CookieConsent\RouteResolver\Contracts\RouteResolver)
    'read-more-route' => 'cookie-consent-read-more', // routename or full url where the cookie information page is located.
    'settings-route' => 'cookie-consent-settings',   // routename or full url where the cookie edit page is located. (leave it as is and it will work out of the box)

    // enable submitting with ajax. NOTE: this requires some more work for integrating
    'enable-ajax-submit' => false,

    // enable/disable categories
    // NOTE: The order wherein the channels here are defined are how they are displayed on the frontend
    'channels-status' => [
        'social' => false,
        'video' => true,
        'tracking' => false,
        'advertisement' => false,
    ],
];
