<?php
/**
 * NOTICE
 *
 * keep this syntax for defining routes
 * When using this format: `Route::any('/cookie-consent', '\Reyez\CookieConsent\Http\Controllers\CookieConsentController@index')->name('cookie-consent');`
 * it breaks in laravel 5.2
 * BUT ALSO when using Manego modules routes
 */

Route::any('/cookie-consent', [
    'as' => 'cookie-consent',
    'uses' => '\Reyez\CookieConsent\Http\Controllers\CookieConsentController@index'
]);

Route::get(config('cookie-consent.settings-route', '/cookie-consent-settings'), [
    'as' => 'cookie-consent-settings',
    'uses' => '\Reyez\CookieConsent\Http\Controllers\CookieConsentController@settings'
]);
